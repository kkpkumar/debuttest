import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(){
    this.formRole();
    this.fromData();
  }


  // ===================== Validation =====================

  myForm: FormGroup;
	email: FormControl;
  pass: FormControl;
  
  formRole() {
    this.email = new FormControl("", [
      Validators.required,
      Validators.pattern(
        "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
      )
    ]);
		this.pass = new FormControl("", [
      Validators.required,
      Validators.pattern(
        ".{8,32}"
      )
    ]);
  }
  fromData() {
		this.myForm = new FormGroup({
			email: this.email,
      pass: this.pass
    })
    console.log(this.myForm.value)
  }
  email_id: string;
  password: string;
  formSubmit(){
    if(this.myForm.valid){
      if(this.email.value == "sunil@gmail.com" && this.pass.value == "admin@123"){
        this.router.navigate(['dashboard']);
        console.log(this.myForm.value);
      }
      else{
        alert('Please enter valid username and password')
      }
    }
    else {
			for (let i in this.myForm.controls) {
				this.myForm.controls[i].markAsTouched();
			}
		}
  }
}
