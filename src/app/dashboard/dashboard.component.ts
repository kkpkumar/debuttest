import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public nameSearch:string = ''

  constructor( private _service: ApiService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  products:any = []
  getProducts(){
    this._service.product().subscribe((data: any ) => {
      this.products = data;
      console.log(this.products)
    },
    error => {}
    );
  }
}
